var dot;

function setup() {
  createCanvas(900, 600);
  dot = new Dot();
}

function draw() {
  background(0);
  dot.run();
}

function Dot() {
  this.speed = createVector(0, 0);
  this.loc = createVector(random(width), random(height));
  this.acceleration = createVector(-0.001, 0.01);
  this.diam = 40;
  this.topSpeed = 5;

  this.run = function() {
    this.draw();
    this.move();
    this.borders();
  }

  this.draw = function() {
    fill(125);
    ellipse(this.loc.x, this.loc.y, this.diam, this.diam);
  }

  this.move = function() {
    var mouse = createVector(mouseX, mouseY);
    var dir = p5.Vector.sub(mouse, this.loc);
    dir.normalize();
    dir.mult(0.3);
    this.acceleration = dir;
    this.speed.add(this.acceleration);
    this.speed.limit(this.topSpeed);
    this.loc.add(this.speed);
  }

  this.borders = function() {
    if (this.loc.x < 0) {
      this.loc.x = width;
    } else if (this.loc.x > width) {
      this.loc.x = 0;
    }

    if (this.loc.y < 0) {
      this.loc.y = height;
    } else if (this.loc.y > height) {
      this.loc.y = 0;
    }
  }
}