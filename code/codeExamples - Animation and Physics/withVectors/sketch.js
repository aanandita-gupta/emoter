var dot;

function setup() {
  createCanvas(900,600);
  dot = new Dot();
}

function draw() {
  background(0);
  dot.run();
}

function Dot() {
  this.velocity = createVector(random(-5,5),random(-5,5));
  this.loc = createVector(random(width), random(height));
  
  this.run = function(){
    this.draw();
    this.move();
    this.borders();
  }
  
  this.draw = function(){
    fill(125);
    ellipse(this.loc.x, this.loc.y, 40, 40);
  }
  
  this.move = function(){
    this.loc.add(this.velocity);
  }
  
  this.borders = function() {
    if (this.loc.x > width) {
      this.loc.x = width;
      this.velocity.x *= -1;
    } else if (this.loc.x < 0) {
      this.velocity.x *= -1;
      this.loc.x = 0;
    }
    if (this.loc.y > height) {
      this.velocity.y *= -1;
      this.loc.y = height;
    } else if (this.loc.y < 0) {
      this.velocity.y *= -1;
      this.loc.y = 0;
    }
  }
}