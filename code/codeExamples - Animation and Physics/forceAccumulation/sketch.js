var dot;

function setup() {
  createCanvas(900, 600);
  dot = new Dot();
}

function draw() {
  background(0);

  var gravity = createVector(0, 0.1);
  var friction = dot.speed.copy();
  friction.mult(-1);
  friction.normalize();
  friction.mult(0.01);
  dot.applyForce(friction);
  dot.applyForce(gravity);

  dot.run();
}

function Dot() {
    this.speed = createVector(1, -3);
    this.loc = createVector(width / 2, height / 2);
    this.acceleration = createVector(0.1, 0);
    this.diam = 40;
    this.topSpeed = 2;


    this.run = function() {
        this.draw();
        this.move();
        this.borders();
    }

    this.draw = function() {
        fill(125);
        ellipse(this.loc.x, this.loc.y, this.diam, this.diam);
    }
    
    this.move = function() {
        this.speed.add(this.acceleration);
        this.loc.add(this.speed);
        this.acceleration.mult(0);
    }

    this.borders = function() {
        if (this.loc.x > width) 
        {
            this.loc.x = width;
            this.speed.x *= -1;
        } 
        else if (this.loc.x < 0)
        {
            this.speed.x *= -1;
            this.loc.x = 0;
        }
        if (this.loc.y > height)
        {
            this.speed.y *= -1;
            this.loc.y = height;
        }
        else if (this.loc.y < 0)
        {
            this.speed.y *= -1;
            this.loc.y = 0;
        }
    }

    this.applyForce = function(f) {
        this.acceleration.add(f);
    }
}