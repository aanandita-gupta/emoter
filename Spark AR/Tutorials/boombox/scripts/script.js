const Animation = require('Animation');
const Scene = require('Scene');
const TouchGestures = require('TouchGestures');

const sceneRoot = Scene.root;

Promise.all([
    sceneRoot.findFirst('base_jnt'),
    sceneRoot.findFirst('speaker_left_jnt'),
    sceneRoot.findFirst('speaker_right_jnt'),
    sceneRoot.findFirst('planeTracker0'),
    sceneRoot.findFirst('placer')
])
.then(function(objects) {
    const base = objects[0];
    const speakerLeft = objects[1];
    const speakerRight = objects[2];
    const planeTracker = objects[3];
    const placer = objects[4];
    
    const baseDriverParameters = {
        durationMilliseconds: 400,
        loopCount: Infinity,
        mirror: true
    };

    const baseDriver = Animation.timeDriver(baseDriverParameters);
    baseDriver.start();

    const baseSampler = Animation.samplers.easeInQuint(0.9,1);

    const baseAnimation = Animation.animate(baseDriver,baseSampler);

    const baseTransform = base.transform;

    // @ts-ignore
    baseTransform.scaleX = baseAnimation;
    // @ts-ignore
    baseTransform.scaleY = baseAnimation;
    // @ts-ignore
    baseTransform.scaleZ = baseAnimation;
    
    const speakerDriverParameters = {
        durationMilliseconds: 200,
        loopCount: Infinity,
        mirror: true
    };

    const speakerDriver = Animation.timeDriver(speakerDriverParameters);
    speakerDriver.start();

    const speakerSampler = Animation.samplers.easeOutElastic(0.7,0.85);

    const speakerAnimation = Animation.animate(speakerDriver,speakerSampler);
    
    const speakerLeftTransform = speakerLeft.transform;

    // @ts-ignore
    speakerLeftTransform.scaleX = speakerAnimation;
    // @ts-ignore
    speakerLeftTransform.scaleY = speakerAnimation;
    // @ts-ignore
    speakerLeftTransform.scaleZ = speakerAnimation;

    const speakerRightTransform = speakerRight.transform;

    // @ts-ignore
    speakerRightTransform.scaleX = speakerAnimation;
    // @ts-ignore
    speakerRightTransform.scaleY = speakerAnimation;
    // @ts-ignore
    speakerRightTransform.scaleZ = speakerAnimation;
    
    TouchGestures.onPan().subscribe(function(gesture) {
        // @ts-ignore
        planeTracker.trackPoint(gesture.location, gesture.state);
    });
    
    const placerTransform = placer.transform;
    
    TouchGestures.onPinch().subscribeWithSnapshot( {
        'lastScaleX' : placerTransform.scaleX,
        'lastScaleY' : placerTransform.scaleY,
        'lastScaleZ' : placerTransform.scaleZ 
    }, function (gesture, snapshot) {
        placerTransform.scaleX = gesture.scale.mul(snapshot.lastScaleX);
        placerTransform.scaleY = gesture.scale.mul(snapshot.lastScaleY);
        placerTransform.scaleZ = gesture.scale.mul(snapshot.lastScaleZ);
    });
    
    TouchGestures.onRotate().subscribeWithSnapshot( {
        'lastRotationY' : placerTransform.rotationY,
    }, function (gesture, snapshot) {
        const correctRotation = gesture.rotation.mul(-1);
        placerTransform.rotationY = correctRotation.add(snapshot.lastRotationY);
    });
});